"""
for windows environment
"""
import argparse
import os
import time
from utils import misc
import torch
from datetime import datetime
import numpy as np
from model import DAE_C, DAE_F, _DAE_R, TC_RES
import train_v3
from source_separation import MFA
from dataset.HLsep_dataloader import hl_dataloader, val_dataloader
import scipy.io.wavfile as wav
from tqdm import tqdm

# parser#
parser = argparse.ArgumentParser(description='PyTorch Source Separation')
parser.add_argument('--model_type', type=str, default='DAE_R', help='model type')
parser.add_argument('--data_feature', type=str, default='lps', help='lps or wavform')

parser.add_argument('--pretrained', default=False, help='load pretrained model or not')
parser.add_argument('--pretrained_path', type=str, default=None, help='pretrained_model path')
parser.add_argument('--trainOrtest', type=str, default="train", help='status of training')
# training hyperparameters
parser.add_argument('--optim', type=str, default="Adam", help='optimizer for training',
                    choices=['RMSprop', 'SGD', 'Adam'])
parser.add_argument('--batch_size', type=int, default=16, help='input batch size for training (default: 64)')
parser.add_argument('--lr', type=int, default=1e-3, help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--CosineAnnealingWarmRestarts', type=bool, default=False,
                    help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--epochs', type=int, default=50, help='number of epochs to train (default: 10)')
parser.add_argument('--grad_scale', type=float, default=8, help='learning rate for wage delta calculation')
parser.add_argument('--seed', type=int, default=117, help='random seed (default: 1)')

parser.add_argument('--log_interval', type=int, default=100,
                    help='how many batches to wait before logging training status')
parser.add_argument('--test_interval', type=int, default=1, help='how many epochs to wait before another test')
parser.add_argument('--logdir', default='log/', help='folder to save to the log')
parser.add_argument('--decreasing_lr', default='200,250', help='decreasing strategy')
# MFA hyperparameters
parser.add_argument('--source_num', type=str, default=3, help='number of separated sources')
parser.add_argument('--clustering_alg', type=str, default='NMF', help='clustering algorithm for embedding space')
parser.add_argument('--wienner_mask', type=bool, default=True, help='wienner time-frequency mask for output')


# 額外的設定
parser.add_argument('--fix_logger_folder_name', default=True, action='store_true', help='fixed logger folder name.')
parser.add_argument('--logger_folder_name', type=str, default='Simple_classifier')

args = parser.parse_args()
args.cuda = torch.cuda.is_available()

misc.logger.init(args.logdir, args.logger_folder_name + '.txt')

if args.fix_logger_folder_name:
    _log_path = os.path.join(args.logdir[:-1], args.logger_folder_name)
    print("log at:{}".format(_log_path))
    args.logdir = os.path.join(args.logdir[:-1], args.logger_folder_name)
    misc.logger.info("fixed logger folder!")
else:
    starttime = time.time()
    current_time = datetime.now().strftime('%Y%m%d_%H%M')
    args.logdir = args.logdir + str(args.model_type) + "_" + str(current_time)
    print("log at:{}".format(args.logdir))
misc.ensure_dir(args.logdir)

logger = misc.logger.info
logger("=================FLAGS==================")
for k, v in args.__dict__.items():
    logger('{}: {}'.format(k, v))
logger("========================================")

"""
if args.seed is not None:
    random.seed(args.seed)
    cudnn.deterministic=None
    ngpus_per_node = torch.cuda.device_count()
"""
# build model
decreasing_lr = list(map(int, args.decreasing_lr.split(',')))
logger('decreasing_lr: ' + str(decreasing_lr))
best_acc, old_file = 0, None
per_save_epoch = 30
t_begin = time.time()
grad_scale = args.grad_scale
# model dictionary
old_DAE_C_dict = {
    "frequency_bins": [0, 300],
    "encoder": [32, 16, 8],
    "decoder": [8, 16, 32, 1],
    "encoder_filter": [[1, 3], [1, 3], [1, 3]],
    "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
    "encoder_act": "relu",
    "decoder_act": "relu",
    "dense": [],
}
# The 蓋
DAE_C_dict = {
    "frequency_bins": [0, 300],
    "encoder": [32, 16, 8],
    "decoder": [8, 16, 32, 1],
    "encoder_filter": [[1, 3], [1, 3], [1, 3]],
    "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
    "encoder_act": "relu",  # sigmoid
    "decoder_act": "relu",  # sigmoid
    "dense": [],
}
DAE_F_dict = {
    "frequency_bins": [0, 300],
    "encoder": [1024, 512, 256, 128],
    "decoder": [256, 512, 1024, 1025],
    "encoder_act": "relu",
    "decoder_act": "relu",
}

Model = {
    'DAE_C': DAE_C.autoencoder,
    'DAE_F': DAE_F.autoencoder,
    'DAE_R': _DAE_R.autoencoder,
    'TC_RES': TC_RES.TC_ResNet
}

model_dict = {
    'DAE_C': DAE_C_dict,
    'DAE_F': DAE_F_dict,
    'DAE_R': DAE_C_dict
}

OLD_FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 2048,
    'Hop_length': 128,
    'Win_length': 2048,
    'normalize': True,
}

#
# eval by matlab
# Resolution: 128 ms
# Overlap: 80%
FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 1024,
    'Hop_length': 205,
    'Win_length': 1024,
    'normalize': True,
}

# declare model
net = Model[args.model_type](model_dict=model_dict[args.model_type], args=args, logger=logger).cuda()

torch.manual_seed(args.seed)
device = None
if args.cuda:
    torch.cuda.manual_seed(args.seed)
    net.cuda()
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

if __name__ == "__main__":
    _data_set = [
        r'.\dataset\audio_Healthy\121_1b1_Tc_sc_Meditron.wav',
    ]

    # data loader
    data_loader = hl_dataloader(_data_set, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                pin_memory=True, FFT_dict=FFT_dict, args=args)
    for data in data_loader:
        print(data)
        print('a')

if __name__ == "__main__887":

    history_list = \
        ["./dataset/0_0.wav",  # 0 0_0
         r'.\dataset\audio_Healthy\102_1b1_Ar_sc_Meditron.wav',  # 1
         r'.\dataset\audio_Healthy\121_1b1_Tc_sc_Meditron.wav',
         r'.\dataset\audio_Healthy\121_1p1_Tc_sc_Meditron.wav',
    ]

    test_filelist = [history_list[2]]
    test_filename = test_filelist[0].rsplit(os.path.sep)[-1].split('.')[0]  #"102_1b1_Ar_sc_Meditron"
    MODEL_SAVE_NAME = test_filename

    model_save_dir = 'model_save'

    go_train = False

    # 檢查是否存在訓練過的 pt檔案，換權重傳薪訓練
    exists_pt = os.path.isfile(os.path.join(model_save_dir, f'{MODEL_SAVE_NAME}.pt'))
    logger(f"\n>>> exists_pt: {exists_pt}")

    always_train = True
    # train
    if always_train:  # not exists_pt
        train_loader = hl_dataloader(test_filelist, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                     pin_memory=True, FFT_dict=FFT_dict, args=args)
        # train
        net = train_v3.train(train_loader, net, args, logger)

        # save
        misc.ensure_folder(model_save_dir, logger=misc.logger)
        torch.save(net.state_dict(), os.path.join(model_save_dir, f'{MODEL_SAVE_NAME}.pt'))


    # data loader
    data_loader = hl_dataloader(test_filelist, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                      pin_memory=True, FFT_dict=FFT_dict, args=args)


    # chk save folder
    model_save_dir = 'model_save'
    misc.ensure_folder(model_save_dir, logger=misc.logger)

    # Reload model
    net.load_state_dict(torch.load(os.path.join(model_save_dir, f'{MODEL_SAVE_NAME}.pt')))
    net.to(device)

    logger('\n==== <net_structure> ====')
    logger('padding tuple meaning:{left, right, top, bottom}')
    logger('conv 使用 寬型(1x3)kernel 所以 padding 只需一邊(橫向)，特別的是統一pad在右邊')
    logger(net)
    logger('==== <net_structure> ====\n')
    outdir = "{0}/test_".format(args.logdir)

    # Source Separation by MFA analysis.
    mfa_old = MFA.MFA_source_separation(net, FFT_dict=FFT_dict, args=args,
                                    logger=logger, use_class3=False)
    mfa_new = MFA.MFA_source_separation(net, FFT_dict=FFT_dict, args=args,
                                        logger=logger, use_class3=True)

    for mfa in [mfa_old, mfa_new]:

        for key, val in FFT_dict.items():
            logger(key + ", " + str(val))
        for test_file in test_filelist:
            # load test data
            lps, phase, mean, std = val_dataloader(test_file, FFT_dict, logger)
            logger("\nval_dataloader 回傳:")
            logger(f"lps.shape:{lps.shape}")
            logger(f"phase.shape:{phase.shape}")
            logger(f"mean.shape:{mean.shape}")
            logger(f"std.shape:{std.shape}")
            logger('\n')
            # 當 mean or std 為 int() 時會有例外吧。

            mfa.source_separation(np.array(lps), np.array(phase), np.array(mean), np.array(std), filedir=outdir,
                                  filename=test_filename)



    # log 重新編碼
    _log_txt = os.path.join('log', args.logger_folder_name + '.txt')
    with open(_log_txt, encoding='big5') as inFile:
        content = inFile.read()
    with open(_log_txt, mode='w+', encoding='utf-8') as outFile:
        outFile.write(content)

