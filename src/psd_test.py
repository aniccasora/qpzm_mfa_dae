from spectrum import Periodogram
import scipy.io.wavfile as wavfile
import matplotlib.pyplot as plt


if __name__ == "__main__":

    audio_path = r"D:\Git\qpzm_mfa_dae\src\dataset\audio_Healthy\121_1b1_Tc_sc_Meditron.wav"

    sr, y = wavfile.read(filename=audio_path, mmap=False)

    p = Periodogram(y, sampling=sr, window='hamming', NFFT=len(y))

    plt.figure(figsize=(20, 5))
    p.plot(marker=',', norm=False, linewidth=2)
    plt.show()