"""
for windows environment
"""
import argparse
import os
import time
from utils import misc
import torch
from torch.autograd import Variable
from datetime import datetime
import numpy as np
from model import DAE_C, DAE_F
import train_v3
from source_separation import MFA
from dataset.HLsep_dataloader import hl_dataloader, val_dataloader
import scipy.io.wavfile as wav
from tqdm import tqdm
from utils.signalprocess import wav2lps, wav2lps_special
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import librosa.display  # 需要顯示導入
import scipy

# parser#

parser = argparse.ArgumentParser(description='PyTorch Source Separation')
parser.add_argument('--model_type', type=str, default='DAE_C', help='model type')
parser.add_argument('--data_feature', type=str, default='lps', help='lps or wavform')

parser.add_argument('--pretrained', default=False, help='load pretrained model or not')
parser.add_argument('--pretrained_path', type=str, default=None, help='pretrained_model path')
parser.add_argument('--trainOrtest', type=str, default="train", help='status of training')
# training hyperparameters
parser.add_argument('--optim', type=str, default="Adam", help='optimizer for training',
                    choices=['RMSprop', 'SGD', 'Adam'])
parser.add_argument('--batch_size', type=int, default=32, help='input batch size for training (default: 64)')
parser.add_argument('--lr', type=int, default=1e-4, help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--CosineAnnealingWarmRestarts', type=bool, default=False,
                    help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--epochs', type=int, default=50, help='number of epochs to train (default: 10)')
parser.add_argument('--grad_scale', type=float, default=8, help='learning rate for wage delta calculation')
parser.add_argument('--seed', type=int, default=117, help='random seed (default: 1)')

parser.add_argument('--log_interval', type=int, default=100,
                    help='how many batches to wait before logging training status')
parser.add_argument('--test_interval', type=int, default=1, help='how many epochs to wait before another test')
parser.add_argument('--logdir', default='log/', help='folder to save to the log')
parser.add_argument('--decreasing_lr', default='200,250', help='decreasing strategy')
# MFA hyperparameters
parser.add_argument('--source_num', type=str, default=3, help='number of separated sources')
parser.add_argument('--clustering_alg', type=str, default='NMF', help='clustering algorithm for embedding space')
parser.add_argument('--wienner_mask', type=bool, default=True, help='wienner time-frequency mask for output')


# 額外的設定
parser.add_argument('--fix_logger_folder_name', default=True, action='store_true', help='fixed logger folder name.')
parser.add_argument('--logger_folder_name', type=str, default='MFA_ANA')
parser.add_argument('--debug_stft', action="store_true")


args = parser.parse_args()

args.cuda = torch.cuda.is_available()

misc.logger.init(args.logdir, args.logger_folder_name + '.txt')

if args.fix_logger_folder_name:
    _log_path = os.path.join(args.logdir[:-1], args.logger_folder_name)
    print("log at:{}".format(_log_path))
    args.logdir = os.path.join(args.logdir[:-1], args.logger_folder_name)
    misc.logger.info("fixed logger folder!")
else:
    starttime = time.time()
    current_time = datetime.now().strftime('%Y%m%d_%H%M')
    args.logdir = args.logdir + str(args.model_type) + "_" + str(current_time)
    print("log at:{}".format(args.logdir))
misc.ensure_dir(args.logdir)

logger = misc.logger.info
logger("=================FLAGS==================")
for k, v in args.__dict__.items():
    logger('{}: {}'.format(k, v))
logger("========================================")

"""
if args.seed is not None:
    random.seed(args.seed)
    cudnn.deterministic=None
    ngpus_per_node = torch.cuda.device_count()
"""
# build model
decreasing_lr = list(map(int, args.decreasing_lr.split(',')))
logger('decreasing_lr: ' + str(decreasing_lr))
best_acc, old_file = 0, None
per_save_epoch = 30
t_begin = time.time()
grad_scale = args.grad_scale
# model dictionary
DAE_C_dict = {
    "frequency_bins": [0, 300],
    "encoder": [32, 16, 8],
    "decoder": [8, 16, 32, 1],
    "encoder_filter": [[1, 3], [1, 3], [1, 3]],
    "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
    "encoder_act": "relu",
    "decoder_act": "relu",
    "dense": [],
}
DAE_F_dict = {
    "frequency_bins": [0, 300],
    "encoder": [1024, 512, 256, 128],
    "decoder": [256, 512, 1024, 1025],
    "encoder_act": "relu",
    "decoder_act": "relu",
}

Model = {
    'DAE_C': DAE_C.autoencoder,
    'DAE_F': DAE_F.autoencoder,
}

model_dict = {
    'DAE_C': DAE_C_dict,
    'DAE_F': DAE_F_dict
}
FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 2048,
    'Hop_length': 128,
    'Win_length': 2048,
    'normalize': True,
}



# declare model
net = Model[args.model_type](model_dict=model_dict[args.model_type], args=args, logger=logger).cuda()

torch.manual_seed(args.seed)
device = None
if args.cuda:
    torch.cuda.manual_seed(args.seed)
    net.cuda()
    device = torch.device('cuda')
else:
    device = torch.device('cpu')


def do_fft(y, sample_rate):
    """

    Args:
        y: 訊號
        sample_rate: 採樣頻率

    Returns:
        xf, yf(實數)
    """
    N = len(y)
    T = 1.0 / sample_rate
    yf = np.fft.fft(y)
    xf = np.fft.fftfreq(N, T)[:(N // 2)]

    return xf, np.abs(yf[0:(N // 2)])

def select_dft_range(signal, start, end, plot_save_path, highlight=False, prefix="", postfix="",
                     fn="框出將要計算計算DFT的範圍"):

    rect_top_bottom_marge = 0.1  # 上下框的額外範圍

    signal_range = signal[start: end + 1]
    #
    fig, ax = plt.subplots(figsize=(12, 5))
    fig.canvas.set_window_title(fn)
    plt.plot(signal, label="audio signal", zorder=0)
    # 決定 方框參數
    max_y = np.max(signal_range) * (1 + rect_top_bottom_marge*2)
    min_y = np.min(signal_range) * (1 + rect_top_bottom_marge*2)
    x_width = len(signal_range)
    y_hight = abs(max_y-min_y)
    rect = patches.Rectangle((start, min_y), x_width, y_hight, linewidth=2, edgecolor='r', facecolor='none')
    ax.add_patch(rect)
    #
    plt.legend()
    if highlight:
        pa5 = (end-start) * 1
        plt.xlim([start-pa5, end+pa5])
    fig.tight_layout()
    if highlight:
        fig.savefig(os.path.join(plot_save_path, f"{prefix+fn+'_highlight'+postfix}.png"))
    else:
        fig.savefig(os.path.join(plot_save_path, f"{prefix + fn + postfix}.png"))


def log_and_nolog_diff(fx, fy, plot_save_path, prefix="", postfix="",
                       fn="有無取 log10 的 dft 結果", log_color='#FF7F0E'):
    fig = plt.figure()
    fig.canvas.set_window_title('取log 與 與不取 log 差別')
    plt.plot(fx, fy, label='dft(x)')
    plt.plot(fx, np.log10(fy**2), label='log10(dft(x)^2)', color=log_color)
    plt.legend()
    fig.savefig(os.path.join(plot_save_path, f"{prefix}{fn}{postfix}.png"))


def just_log10(fx, fy, plot_save_path, prefix="", postfix="", color='#FF7F0E',
               fn="平方後取log10"):
    """

    Args:
        y:
        plot_save_path:
        prefix:
        postfix:
        color:  default橘色

    Returns:

    """
    fig = plt.figure()
    fig.canvas.set_window_title(fn)
    plt.plot(fx, np.log10(fy**2), label='log10(dft(x)^2)', color=color)
    plt.legend()
    fig.savefig(os.path.join(plot_save_path, f"{prefix}{fn}{postfix}.png"))


def just_dft(fx, fy, plot_save_path, prefix="", postfix="",
             fn='針對要做範圍做 DFT 的結果圖'):
    fig = plt.figure()
    fig.canvas.set_window_title(fn)
    plt.plot(fx, fy, label='dft(x)')
    plt.legend()
    fig.savefig(os.path.join(plot_save_path, f"{prefix}{fn}{postfix}.png"))


def get_small_fx_fy(fx, fy, window_length):
    """
    Args:
        fx: 應該與 fy shape 相同
        fy: 應該與 fx shape 相同
        window_length:

    Returns:
        較少量的 fx 與 fy。
    """
    assert fx.shape == fy.shape
    select = window_length//2

    select_T = len(fx) // select
    new_x, new_y = [], []

    for idx in range(0, len(fx), select_T):
        new_x.append(fx[idx])
        new_y.append(fy[idx])

    return np.asarray(new_x), np.asarray(new_y)


def draw_spectrum(fft_result_list, all_range_fft_result, plot_save_path,
                   each_column_double_time=6, prefix="", postfix="", fn="自組頻譜圖"):
    """

    Args:
        fft_result_list:   [fft 的結果 list]
        all_range_fft_result: 整段音訊的 fft 結果
        plot_save_path: 存檔路徑
        each_column_double_time:  每個column要變粗幾倍(2的冪次方倍)
        prefix:
        postfix:
        fn: 存檔檔名

    Returns:
        畫張 sprectrum
    """
    # 讓 column 變粗，注意 是 指數成長
    double_col = lambda col: np.concatenate((col, col), axis=1)

    spectrum = None

    for fft_result in fft_result_list:
        column_fft = fft_result.reshape(-1, 1)[::-1, :]  # 相反用，不然頻譜 y-axis 下方不是低頻率。
        for _ in range(each_column_double_time):
            column_fft = double_col(column_fft)
        if spectrum is None:
            spectrum = column_fft
        else:
            spectrum = np.concatenate((spectrum, column_fft), axis=1)
    # ---
    fig, ax = plt.subplots()
    fig.canvas.set_window_title('DIY頻譜圖!!!')
    im = ax.imshow(spectrum,
                   cmap=plt.cm.plasma,
                   vmin=np.min(all_range_fft_result),
                   vmax=np.max(all_range_fft_result))
    fig.colorbar(im, ax=ax)
    fig.savefig(os.path.join(plot_save_path, f"{prefix + fn + postfix}.png"))

def save_stft_spectrum(wav_path, plot_save_path, prefix="", fn="官方spectrum", postfix=""):

    sr, y = wav.read(wav_path)
    y = y / 32767.

    S = np.abs(librosa.stft(y=y,
                            n_fft=FFT_dict['FFTSize'],
                            hop_length=FFT_dict['Hop_length'],
                            win_length=FFT_dict['Win_length'],
                            window=scipy.signal.hamming,  # "hann"
                            center=False,
                            dtype=None,
                            pad_mode="reflect")
               )

    #
    # step_yo = [1, 2, 3, 4, 5, 6, S.shape[1]]
    # for spectrum_selected_bins in step_yo:  # range(1, S.shape[1], 1)
    #     fig, ax = plt.subplots()
    #     fig.canvas.set_window_title(f"width:{spectrum_selected_bins}")
    #     short_S = S[:, :spectrum_selected_bins]  # 取道第 [0~N] 個 column 而已
    #     short_S = short_S[::-1, :]  # 上下相反
    #
    #     # 轉 db
    #     spectrum = librosa.amplitude_to_db(short_S,
    #                                        ref=np.max,
    #                                        amin=1e-5,
    #                                        top_db=70.0)
    #
    #     # 將單條變粗用
    #     double_col = lambda col: np.concatenate((col, col), axis=1)
    #
    #     more_widthly_spectrum = None
    #
    #     for single_bin_idx in range(spectrum.shape[1]):
    #         single_bin = spectrum[:, single_bin_idx].reshape(-1, 1)
    #         for _ in range(4):  # 單column變粗
    #             single_bin = double_col(single_bin)
    #         if more_widthly_spectrum is None:
    #             more_widthly_spectrum = single_bin
    #         else:
    #             more_widthly_spectrum = np.concatenate((more_widthly_spectrum, single_bin), axis=1)
    #
    #     im = ax.imshow(more_widthly_spectrum,
    #               cmap=plt.cm.plasma,
    #               vmin=np.min(spectrum),
    #               vmax=np.max(spectrum))
    #     fig.colorbar(im, ax=ax)
    #     plt.axis('off')
    #     plt.subplots_adjust()
    #     plt.show()
    # 官方繪製 頻譜圖
    fig, ax = plt.subplots(figsize=(50, 10))
    fig.canvas.set_window_title(f"官方spectrum")
    spectrum = librosa.amplitude_to_db(S,
                                       ref=np.max,
                                       amin=1e-5,
                                       top_db=70.0)
    img = librosa.display.specshow(spectrum,
                                   sr=8000,
                                   hop_length=FFT_dict['Hop_length'],
                                   y_axis='log', x_axis='time', ax=ax)
    ax.set_title(f'Power spectrogram')
    fig.colorbar(img, ax=ax, format="%+2.0f dB")

    plt.tight_layout()
    fig.savefig(os.path.join(plot_save_path, f"{prefix + fn + postfix}.png"))
    #   ------ def function() end


if __name__ == "__main__":

    args.debug_stft = True

    filepath = r"D:\Git\qpzm_mfa_dae\src\dataset\audio_Healthy\121_1b1_Tc_sc_Meditron.wav"

    plot_save_path = r'D:\Git\qpzm_mfa_dae\src\plots'

    spec, phase, mean, std = wav2lps(filepath,
                                     FFT_dict['FFTSize'],
                                     FFT_dict['Hop_length'],
                                     FFT_dict['Win_length'],
                                     FFT_dict['normalize'])

    # 利用 librosa 來產出 stft spectrum.
    save_stft_spectrum(filepath, plot_save_path)

    print(spec.shape)

    STFT_PARAM = {
        'sr': 8000,
        'FFTSize': 2048,
        'Hop_length': 128,
        'Win_length': 2048,
        'normalize': True,
    }

    sr, y = wav.read(filepath)
    y = y / 32767.

    sub_y = y[0:2048]

    # 對全時間做 dft
    fx, fy = do_fft(y, sample_rate=sr)

    # 對所要區塊 算  dft
    fx_sub, fy_sub = do_fft(sub_y, sample_rate=sr)

    # 全時間 dft 取 log(X**2)
    # 畫 spectrum 用。
    fy_log = np.log10(fy**2)

    # 畫出原始音訊
    fig = plt.figure(figsize=(12, 5))
    fig.canvas.set_window_title('原始音訊')
    plt.plot(y, label="audio signal")
    plt.legend()
    fig.tight_layout()
    fig.savefig(os.path.join(plot_save_path, "原始音訊.png"))

    bye = True
    if bye:
        # 在原始訊號中 框出一個範圍
        select_dft_range(signal=y, start=0, end=2048, highlight=False, prefix="0_1_", plot_save_path=plot_save_path)

        # 在原始訊號中 框出一個範圍 不會看到總範圍。
        select_dft_range(signal=y, start=0, end=2048, highlight=True, prefix="0_2_", plot_save_path=plot_save_path)

        # dft 結果 (子範圍)
        just_dft(fx_sub, fy_sub, prefix="0_3_", plot_save_path=plot_save_path)

        # 劃出有 log 與 無 log 的 dft 結果 (子範圍)
        log_and_nolog_diff(fx_sub, fy_sub, prefix="0_4_", plot_save_path=plot_save_path, log_color='#FF7F0E')

        # 取 log 的結果  (子範圍)
        just_log10(fx_sub, fy_sub, prefix="0_5_", plot_save_path=plot_save_path, color='#FF7F0E')

        # 數樣本較少的版本 組頻譜圖一律用這個版本的 fft 結果。
        # 因為原始版本的太多了我們只要 4000 個點即可，因為採樣頻率是 8000Hz
        # 在畫 fft 結果圖就用 4000點畫就好了，不用到 40000點，這樣...
        sfx_sub, sfy_sub = get_small_fx_fy(fx_sub, fy_sub, window_length=STFT_PARAM['FFTSize'])

        # 畫張 spectrum
        # 沒用摟~
        useful = True
        if not useful:
            draw_spectrum(fft_result_list=[np.log10(sfy_sub ** 2), np.log10((sfy_sub+10) ** 2)],
                          all_range_fft_result=fy_log, plot_save_path=plot_save_path, prefix="0_6_")

    # log 重新編碼
    _log_txt = os.path.join('log', args.logger_folder_name + '.txt')
    with open(_log_txt, encoding='big5') as inFile:
        content = inFile.read()
    with open(_log_txt, mode='w+', encoding='utf-8') as outFile:
        outFile.write(content)

