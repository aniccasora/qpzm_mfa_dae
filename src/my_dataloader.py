import os
from torch.utils.data import Dataset
from typing import List
from os.path import join as pjoin
import scipy.io.wavfile as wav
import numpy as np
import librosa
import scipy

def process_dir_tree(path: str) -> List[str]:
    """
    回傳 path目錄下的 "第一層目錄" 的所有檔案的完整路徑。(目錄樹下的)
    """
    res = []
    for idx, (current_path, dirs, files) in enumerate(os.walk(path)):
        # print(f"此目錄的 第 {idx + 1} 個資料夾 {current_path}:")
        #走訪 path 下資料夾下的所有檔案。
        for filename in files:
            res.append(pjoin(path, current_path, filename))

    # test : 檢測所有檔案是存在的
    for _ in res:
        assert os.path.exists(_)

    return res

class SplitUpDataset(Dataset):
    """
    給處理過的(按照他dataset給的label時間段的去割成一段一段的片段)
    心肺音 dataloader

    label:
        0: normal
        1: wheezes
        2: crackles
        3: wheezes & crackles
    """
    def __init__(self, splited_audio_path, **kwargs):
        # 資料夾下的所有檔案
        self.audio_namelist = process_dir_tree(splited_audio_path)
        self.audio_ptr = None  # 只是一個 audio 的路徑
        self.kwargs = kwargs

        # check
        assert isinstance(self.audio_namelist, List)

    def audio2Spectrogram(self, **kwargs):
        assert self.audio_ptr is not None
        """
        to log power spectrum.
        """
        if 'FFTSize' not in self.kwargs:
            self.kwargs['FFTSize'] = 512

        if 'hop_length' not in self.kwargs:
            self.kwargs['hop_length'] = 256

        if 'win_length' not in self.kwargs:
            self.kwargs['win_length'] = 512

        if 'center' not in self.kwargs:
            self.kwargs['center'] = False

        if type(self.audio_ptr) == str:
            sr, y = wav.read(self.audio_ptr)

            if np.max(y) < 2**15-1 and np.min(y) > -(2**15):
                y = y / 32767.
        else:
            assert print("self.audio_ptr 必須是 str")

        epsilon = np.finfo(float).eps

        D = librosa.stft(y, n_fft=self.kwargs['FFTSize'],
                         hop_length=self.kwargs['hop_length'],
                         win_length=self.kwargs['win_length'],
                         window=scipy.signal.hamming,
                         center=self.kwargs['center'])
        D = D + epsilon
        Sxx = np.log10(abs(D) ** 2)
        phase = np.exp(1j * np.angle(D))

        if True:  # 總是使用正規化
            mean = np.mean(Sxx, axis=1).reshape(-1, 1)
            std = np.std(Sxx, axis=1, dtype=np.float32, ddof=1).reshape(-1, 1)
            Sxx = (Sxx - mean) / std
            return {"Sxx": Sxx, "phase": phase, "mean": mean, "std": std}
        else:
            return {"Sxx": Sxx, "phase": phase, "mean": 0, "std": 1}

    def __getitem__(self, item):
        idx = (item % 50)
        self.audio_ptr = self.audio_namelist[idx]
        label = self._get_label_by_current_audio_ptr()

        return self.audio2Spectrogram(), label

    def __len__(self):
        return len(self.audio_namelist)

    def _get_label_by_current_audio_ptr(self):
        #  檔名上面就有 label
        token = self.audio_ptr.split(os.sep)[-1].split('.')[0].strip('_').split('_')[-1]
        res = -1
        if token == 'w':
            res = 1
        elif token == 'c':
            res = 2
        elif token == 'wc' or token == 'cw':
            res = 3
        elif token == 'n':
            res = 0
        else:
            raise ValueError("檔名 不存在 label.\npath=> {}".format(self.audio_ptr))

        return res



if __name__ == "__main__":
    #
    the_cutted = r'D:\Git\qpzm_mfa_dae\src\wav_sliceing\split_labeled_wav'
    data = SplitUpDataset(the_cutted, FFTSize=2048, hop_length=128, win_length=2048)
    stft_res, label = data[50]
    pass