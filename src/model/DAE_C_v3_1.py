# -*- coding: utf-8 -*-
#Author:Wei-Chien Wang

"""
改用  週期性學習能力 的版本。
"""

import numpy as np
import os
import torch
import torch.nn as nn
import source_separation.MFA_2 as MFA
import math


def ACT(act_f):
    if act_f == "relu":
        return torch.nn.ReLU()
    elif act_f == "tanh":
        return torch.nn.Tanh()
    elif act_f == "relu6":
        return nn.ReLU6()
    elif act_f == "sigmoid":
        return nn.Sigmoid()
    elif act_f == "LeakyReLU":
        return nn.LeakyReLU()
    else:
        raise ValueError("Doesn't support {0} activation function".format(act_f))

def _get_entire_spec_mode(self_obj, args):
    if args is None:
        return False
    else:
        return self_obj.args.entire_spec_mode

class Encoder(nn.Module):

    def __init__(self, model_dict=None, padding="same", args=None, logger=None):
        super(Encoder, self).__init__()
        self.model_dict = model_dict
        self.padding = padding
        self.feature_dim = self.model_dict['frequency_bins'][1] - self.model_dict['frequency_bins'][0]
        self.encoder_act = self.model_dict['encoder_act']
        self.encoder_layer = self.model_dict['encoder']
        self.encoder_filter = self.model_dict['encoder_filter']
        self.dense_layer = self.model_dict['dense']
        self.args = args
        # 這邊可以怎麼改 結果跑得比較好看?
        self.first_layer_of_filters = None  # 不使用 設定 None 即可
        # self.diff_filter_size_conv_layers = self._make_sp_layers()
        self.conv_layers = self._make_layers()
        # self.entire_spec_mode = args.entire_spec_mode
        self.entire_spec_mode = _get_entire_spec_mode(self, self.args)
        # v3
        self.Period = pc(args)
        self.latent_act = ACT(self.encoder_act)

        if len(self.dense_layer) != 0:
            if self.padding == "same":
                input_dim = self.encoder_layer[-1]*self.feature_dim
                self.dense_layer = nn.Linear(input_dim, self.dense_layer[0])
                self.dense_act = ACT(self.encoder_act)
            else:
                s_ = np.sum(np.array(self.encoder_filter), axis = 1) - np.array(self.encoder_filter).shape[1]
                input_dim = self.encoder_layer[-1]*(self.feature_dim - s_)
                self.dense_layer = nn.Linear(input_dim, self.dense_layer[0])
                self.dense_act = ACT(self.encoder_act)
        else:
            # 沒有 dense layer.
            # 一般都走這邊。
            pass
        # v3 update
        self.latent_pad_layer = nn.ZeroPad2d(
            padding=(0, self.encoder_filter[-1][1] - 1, 0, self.encoder_filter[-1][0] - 1))
        #
        self.latent_layer = nn.Conv2d(self.encoder_layer[-2], self.encoder_layer[-1],
                                      kernel_size=(self.encoder_filter[-1][0], self.encoder_filter[-1][1]),
                                      stride=(1, 1), padding=0, bias=True)

    # def _make_sp_layers(self):
    #     if self.first_layer_of_filters is None:
    #         return None
    #
    #     layers = []
    #     for filter_n in self.first_layer_of_filters:
    #         layers.append(nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(1, filter_n),
    #                        stride=(1, 1), padding="same", padding_mode='zeros',
    #                        device='cuda'))
    #     return nn.ModuleList(layers)

    def _make_layers(self):
        layers = []
        in_channels = 1

        for i in range(0, len(self.encoder_layer)-1):
            out_channels = self.encoder_layer[i]
            pad_layer = nn.ZeroPad2d(padding=(0, self.encoder_filter[i][1] - 1, 0, self.encoder_filter[i][0] - 1))
            if (self.padding == "same"):
                layers.append(pad_layer)
            encoder_layer = nn.Conv2d(in_channels, out_channels,
                                      kernel_size=(self.encoder_filter[i][0], self.encoder_filter[i][1]), stride=(1, 1),
                                      padding=0, bias=True)

            in_channels = out_channels
            layers.append(encoder_layer)
            layers.append(ACT(self.encoder_act))

        return nn.Sequential(*layers)


    def forward(self, x):

        # if self.diff_filter_size_conv_layers is not None:
        #     # 將輸入X 丟入不同filter size 的 conv
        #     _tmp_x = []
        #     for i, conv2d in enumerate(self.diff_filter_size_conv_layers):
        #         _tmp_x.append(conv2d(x))
        #     x = torch.cat(_tmp_x, dim=1)

        x = self.conv_layers(x)
        x = self.latent_pad_layer(x)
        x = self.latent_layer(x)
        x = torch.reshape(x, (x.shape[0], -1))

        if(len(self.model_dict["dense"])!=0):
            x = self.dense_layer(x)
            x = self.dense_act(x)

        x = self.latent_act(x)
        if torch.any(x.isnan()).item():
            raise ValueError("nan Error -> x")
        x, xr, labels = self.Period(x + 1e-5)

        return x, xr, labels


class pc(nn.Module):

    def __init__(self, args):
        super(pc, self).__init__()
        # watch out
        FFT_dict = {
            'sr': 8000,
            'frequency_bins': [0, 1024],
            'FFTSize': 2048,
            'Hop_length': 128,
            'Win_length': 2048,
            'normalize': True,
        }

        self.mfa = MFA.MFA_source_separation(FFT_dict=FFT_dict, args=args)

    def forward(self, x):
        x = torch.fft.fft(x, dim=-1)
        x_r = torch.abs(x)**2
        x_c = torch.exp(1j * torch.angle(x)).detach()
        x_r = torch.sqrt(x_r)
        x = torch.multiply(x_r, x_c).detach()
        x = torch.fft.ifft(x, dim=-1).detach()
        x = torch.real(x).detach()
        x_r_detach = x_r.detach()
        k_labels = self.mfa.MFA(x_r_detach.cpu().numpy(), source_num=2)
        #print(x[:,k_labels==0].shape)
        #print(k_labels==0)
        return x, x_r, k_labels


class Decoder(nn.Module):
    ###
    def __init__(self, model_dict, padding="same", args=None, logger=None):
        super(Decoder, self).__init__()
        self.model_dict = model_dict
        self.dense_l = self.model_dict['dense']
        self.feature_dim = self.model_dict['frequency_bins'][1] - self.model_dict['frequency_bins'][0]
        self.decoder_act = self.model_dict['decoder_act']
        self.decoder_layer = self.model_dict['decoder']
        self.decoder_filter = self.model_dict['decoder_filter']
        self.args = args
        self.conv_layers = self._make_layers()
        if(len(self.dense_l)!=0):
            self.dense_layer = nn.Linear(self.dense_l[0], self.feature_dim)
            self.dense_act = ACT(self.decoder_act)


    def _make_layers(self):
        layers = []

        if len(self.dense_l) == 0:
            in_channels = self.decoder_layer[0]
        else:
            in_channels = 1

        for i in range(0, len(self.decoder_layer)):
            out_channels = self.decoder_layer[i]
            if i == (len(self.decoder_layer) - 1):
                decoder_layer = nn.Conv2d(in_channels, out_channels,
                                          kernel_size=(self.decoder_filter[i][0], self.decoder_filter[i][1]),
                                          stride=(1, 1), padding=0, bias=True)
                layers.append(decoder_layer)
            else:
                decoder_layer = nn.ConvTranspose2d(in_channels, out_channels,
                                                   kernel_size=(self.decoder_filter[i][0], self.decoder_filter[i][1]),
                                                   stride=(1, 1), padding=(0, 1))
                layers.append(decoder_layer)
                layers.append(ACT(self.decoder_act))
            in_channels = out_channels
        return nn.Sequential(*layers)


    def forward(self, x):

        if len(self.dense_l) == 0: 
            x = torch.reshape(x, (-1, self.decoder_layer[0], 1, self.feature_dim))
        else:
            x = self.dense_layer(x)
            x = self.dense_act(x)
            x = torch.reshape(x, (-1, 1, 1, self.feature_dim))
        x = self.conv_layers(x)

        return x


class autoencoder(nn.Module):

    """
    # deep convolutional autoencoder
    """

    def __init__(self, model_dict=None, padding="same", args=None, logger=None):
        super(autoencoder, self).__init__()
        if model_dict==None:
            self.model_dict = {
                "frequency_bins": [0, 257],
                "encoder": [1024, 512, 256, 32],
                "decoder": [32, 256, 512, 1024, 1],
                "encoder_filter": [[1, 3],[1, 3],[1, 3],[1, 3]],
                "decoder_filter": [[1, 3],[1, 3],[1, 3],[1, 3],[1, 1]],
                "encoder_act": "relu",
                "decoder_act": "relu",
                "dense": [16],
                }
        else:
            self.model_dict = model_dict
        self.feature_dim = self.model_dict['frequency_bins'][1] - self.model_dict['frequency_bins'][0]
        self.encoder = Encoder(self.model_dict, args=args)
        self.decoder = Decoder(self.model_dict, args=args)


    def forward(self, x):

        x, _, _ = self.encoder(x)
        x = self.decoder(x)

        return x

if __name__=="__main__":
    #####test phasea
    x = torch.tensor(np.ones((10, 1, 1, 257))).float()
    model_dict = {
        "frequency_bins":[0, 257],
        "encoder":[1024, 512, 256, 32],
        "decoder":[32, 256, 512, 1024, 1],
        "encoder_filter":[[1,3],[1,3],[1,3],[1,3]],
        "decoder_filter":[[1,3],[1,3],[1,3],[1,3],[1,1]],
        "encoder_act":"relu",
        "decoder_act":"relu",
        "dense":[],
        }
    
    encoder = Encoder(model_dict)
    decoder = Decoder(model_dict)
    output = encoder(x)
    print(output.shape)
    x_ = decoder(output)
    print(x_.shape)
    
    model = autoencoder()
    y = model(x)
    print("output.shape", y.shape)
    l = model.encoder(x)
    print("l.shape", l.shape)
    y = model.decoder(l)
    print("y.shape", y.shape)

