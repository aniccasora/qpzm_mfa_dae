# -*- coding: utf-8 -*-
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

def ACT(act_f):
    if act_f == "relu":
        return torch.nn.ReLU()
    elif act_f == "tanh":
        return torch.nn.Tanh()
    elif act_f == "relu6":
        return nn.ReLU6()
    elif act_f == "sigmoid":
        return nn.Sigmoid()
    elif act_f == "LeakyReLU":
        return nn.LeakyReLU()
    else:
        raise ValueError("Doesn't support {0} activation function".format(act_f))

class BasicEncoderBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1):
        super(BasicEncoderBlock, self).__init__()
        self.pad_layer1 = nn.ZeroPad2d(
            padding=(0,  # left
                     2,  # right,  self.encoder_filter[idx][1] - 1
                     0,  # top
                     0)  # bottom,  self.encoder_filter[idx][0] - 1
        )
        self.conv1 = nn.Conv2d(
            in_planes, planes, kernel_size=(1, 3), stride=stride, padding=0, bias=False)
        #self.bn1 = nn.BatchNorm2d(planes)
        self.pad_layer2 = nn.ZeroPad2d(
            padding=(0,  # left
                     2,  # right,  self.encoder_filter[idx][1] - 1
                     0,  # top
                     0)  # bottom,  self.encoder_filter[idx][0] - 1
        )
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=(1, 3),
                               stride=1, padding=0, bias=False)
        #self.bn2 = nn.BatchNorm2d(planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )

    def forward(self, x):
        out = self.pad_layer1(x)  # 先 pad. 自己++
        out = self.conv1(out)
        #out = self.bn1(out)
        out = F.relu(out)

        out = self.pad_layer2(out)  # 先 pad. 自己++
        out = self.conv2(out)
        #out = self.bn2(out)

        out += self.shortcut(x)

        out = F.relu(out)
        return out

class ResNetEncoder(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10):
        super(ResNetEncoder, self).__init__()
        self.in_planes = 1
        self.pad_layer1 = nn.ZeroPad2d(
            padding=(0,  # left
                     2,  # right,  self.encoder_filter[idx][1] - 1
                     0,  # top
                     0)  # bottom,  self.encoder_filter[idx][0] - 1
        )
        self.conv1 = nn.Conv2d(1, 1, kernel_size=(1, 3),
                               stride=1, padding=0, bias=False)
        #self.bn1 = nn.BatchNorm2d(1)
        self.layer1 = self._make_layer(block, 1, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 32, num_blocks[1], stride=1)
        self.layer3 = self._make_layer(block, 16, num_blocks[2], stride=1)
        self.layer4 = self._make_layer(block, 8, num_blocks[3], stride=1)
        # self.linear = nn.Linear(8*block.expansion, num_classes)


    def _make_layer(self, block, planes, num_blocks, stride):
        # planes: 輸出的 channel 數。
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:  # 根據每層的 num_blocks 走訪
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.pad_layer1(x)#  先 pad, 自己++
        out = self.conv1(out)
        #out = self.bn1(out)
        out = F.relu(out)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        # out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        # out = self.linear(out)
        return out


def ConvTranspose2d_in_out(in_planes, out_planes, kernel_size=(1, 3), stride=(1, 1), padding=(0, 1)):
    return nn.ConvTranspose2d(in_planes, out_planes,
                              kernel_size=kernel_size,
                              stride=stride,
                              padding=padding)


class ResNetDecoder(nn.Module):
    def __init__(self, block, num_blocks):
        super(ResNetDecoder, self).__init__()
        self.first_in_in_planes = 8  # hardcode, 這個固定
        self.in_planes = 8  # hardcode, 這會給 _make_layer 迭代改值。

        self.layer1 = self._make_layer(block, 8, num_blocks[0], stride=1)  # 第二參數是out planes 數
        self.layer2 = self._make_layer(block, 16, num_blocks[1], stride=1)
        self.layer3 = self._make_layer(block, 32, num_blocks[2], stride=1)
        self.layer4 = self._make_layer(block, 1, num_blocks[3], stride=1)


    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        x = x.view(-1, self.first_in_in_planes, 1, 300)
        out = self.layer1(x)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        #  out.size(0) batch 的意思，
        out = out.view(out.size(0), 1, 1, -1)
        # out = self.linear(out)
        return out


class BasicDecoderBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1):
        super(BasicDecoderBlock, self).__init__()

        self.convTranspose1 = ConvTranspose2d_in_out(in_planes, planes)
        #self.bn1 = nn.BatchNorm2d(planes)

        self.convTranspose2 = ConvTranspose2d_in_out(planes, planes)
        #self.bn2 = nn.BatchNorm2d(planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )

    def forward(self, x):

        out = self.convTranspose1(x)
        #out = self.bn1(out)
        out = F.relu(out)

        out = self.convTranspose2(out)
        #out = self.bn2(out)

        out += self.shortcut(x)

        out = F.relu(out)
        return out


class Encoder(nn.Module):

    def __init__(self, model_dict=None, padding="same", args=None, logger=None):
        super(Encoder, self).__init__()
        self.model_dict = model_dict
        self.padding = padding
        self.feature_dim = self.model_dict['frequency_bins'][1] - self.model_dict['frequency_bins'][0]
        self.encoder_act = self.model_dict['encoder_act']
        self.encoder_layer = self.model_dict['encoder']
        self.encoder_filter = self.model_dict['encoder_filter']
        self.dense_layer = self.model_dict['dense']
        self.relu = nn.ReLU(inplace=True)  # for res
        self.shortcut = nn.Sequential()  # for res
        # 防止耍笨
        assert len(self.encoder_layer) == len(self.encoder_filter)  # 幾個 Conv層 就有幾個 filter。
        # ====
        self.conv_layers = self._make_layers()

        if len(self.dense_layer) != 0:
            if self.padding == "same":
                input_dim = self.encoder_layer[-1] * self.feature_dim
                self.dense_layer = nn.Linear(input_dim, self.dense_layer[0])
                self.dense_act = ACT(self.encoder_act)
            else:
                s_ = np.sum(np.array(self.encoder_filter), axis=1) - np.array(self.encoder_filter).shape[1]
                input_dim = self.encoder_layer[-1] * (self.feature_dim - s_)
                self.dense_layer = nn.Linear(input_dim, self.dense_layer[0])
                self.dense_act = ACT(self.encoder_act)

    def _make_layers(self):

        layers = []
        in_channels = 1  # 因為輸入的 spectrogram 為 1-channel.

        # 根據層數 去 loop 造 layer
        for idx, layer_size in enumerate(self.encoder_layer):
            out_channels = layer_size

            if self.padding == "same":
                pad_layer = nn.ZeroPad2d(
                    padding=(0,  # left
                             self.encoder_filter[idx][1] - 1,  # right
                             0,  # top
                             self.encoder_filter[idx][0] - 1)  # bottom
                )
                layers.append(pad_layer)
            encoder_layer = nn.Conv2d(in_channels, out_channels,
                                      kernel_size=(self.encoder_filter[idx][0], self.encoder_filter[idx][1]), stride=(1, 1),
                                      padding=0, bias=True)
            layers.append(encoder_layer)
            #layers.append(nn.BatchNorm2d(out_channels))

            layers.append(ACT(self.encoder_act))  # 選擇 activate function.


            in_channels = out_channels  # 此層的輸出 channel 為下一層的輸出。
            # GO TO APPEND NEXT LAYER.

        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv_layers(x)
        out = torch.reshape(out, (out.shape[0], -1))
        if len(self.model_dict["dense"]) != 0:
            out = self.dense_layer(out)
            out = self.dense_act(out)
        return out

class Decoder(nn.Module):
    ###
    def __init__(self, model_dict, padding="same", args=None, logger=None):
        super(Decoder, self).__init__()
        self.model_dict = model_dict
        self.dense_l = self.model_dict['dense']
        self.feature_dim = self.model_dict['frequency_bins'][1] - self.model_dict['frequency_bins'][0]
        self.decoder_act = self.model_dict['decoder_act']
        self.decoder_layer = self.model_dict['decoder']
        self.decoder_filter = self.model_dict['decoder_filter']
        self.conv_layers = self._make_layers()
        self.relu = nn.ReLU(inplace=True)  # for res
        self.shortcut = nn.Sequential()  # for res
        # 防止耍笨
        assert len(self.decoder_layer) == len(self.decoder_filter)  # 幾個 Conv層 就有幾個 filter。

        if (len(self.dense_l) != 0):
            self.dense_layer = nn.Linear(self.dense_l[0], self.feature_dim)
            self.dense_act = ACT(self.decoder_act)

    def _make_layers(self):

        layers = []

        if len(self.dense_l) == 0:
            in_channels = self.decoder_layer[0]
        else:
            in_channels = 1

        for i in range(0, len(self.decoder_layer)):
            out_channels = self.decoder_layer[i]
            if i == (len(self.decoder_layer) - 1):
                decoder_layer = nn.Conv2d(in_channels, out_channels,
                                          kernel_size=(self.decoder_filter[i][0], self.decoder_filter[i][1]),
                                          stride=(1, 1), padding=0, bias=True)
                layers.append(decoder_layer)
                #layers.append(nn.BatchNorm2d(out_channels))

            else:
                decoder_layer = nn.ConvTranspose2d(in_channels, out_channels,
                                                   kernel_size=(self.decoder_filter[i][0], self.decoder_filter[i][1]),
                                                   stride=(1, 1), padding=(0, 1))
                layers.append(decoder_layer)
                #layers.append(nn.BatchNorm2d(out_channels))
                layers.append(ACT(self.decoder_act))


            in_channels = out_channels
        return nn.Sequential(*layers)

    def forward(self, x):

        if len(self.dense_l) == 0:
            x = torch.reshape(x, (-1, self.decoder_layer[0], 1, self.feature_dim))
        else:
            x = self.dense_layer(x)
            x = self.dense_act(x)
            x = torch.reshape(x, (-1, 1, 1, self.feature_dim))
        x = self.conv_layers(x)

        return x


class autoencoder(nn.Module):
    """
    # deep convolutional autoencoder
    """

    def __init__(self, model_dict=None, padding="same", args=None, logger=None):
        super(autoencoder, self).__init__()
        if model_dict == None:
            self.model_dict = {
                "frequency_bins": [0, 257],
                "encoder": [1024, 512, 256, 32],
                "decoder": [32, 256, 512, 1024, 1],
                "encoder_filter": [[1, 3], [1, 3], [1, 3], [1, 3]],
                "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 3], [1, 1]],
                "encoder_act": "relu",
                "decoder_act": "relu",
                "dense": [16],
            }
        else:
            self.model_dict = model_dict
        self.feature_dim = self.model_dict['frequency_bins'][1] - self.model_dict['frequency_bins'][0]

        # FIXME:裡面是hardcode，上面的參數不會用
        self.encoder = ResNetEncoder(BasicEncoderBlock, [1, 1, 1, 1])  # Encoder(self.model_dict) # res 改
        self.decoder = ResNetDecoder(BasicDecoderBlock, [1, 1, 1, 1])  # Decoder(self.model_dict) # res 改

    def forward(self, x):

        x = self.encoder(x)
        x = self.decoder(x)  # torch.Size([10, 2400])

        return x


if __name__ == "__main__":
    #####test phasea
    resEncoder = ResNetEncoder(BasicEncoderBlock, [2, 2, 2, 2])
    resDecoder = ResNetDecoder(BasicDecoderBlock, [2, 2, 2, 2])

    x = torch.tensor(np.ones((10, 1, 1, 300))).float()

    model_dict ={
        "frequency_bins": [0, 300],
        "encoder": [32, 16, 8],
        "decoder": [8, 16, 32, 1],
        "encoder_filter": [[1, 3], [1, 3], [1, 3]],
        "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
        "encoder_act": "relu",
        "decoder_act": "relu",
        "dense": [],
    }

    resAE = autoencoder(model_dict=model_dict)
    y = resAE(x)
    print(y.size())


    # 下方是舊版 non-res
    encoder = Encoder(model_dict)
    decoder = Decoder(model_dict)

    output = encoder(x)
    print(output.shape)

    x_ = decoder(output)
    print(x_.shape)

    model = autoencoder(model_dict=model_dict)
    y = model(x)
    print("output.shape", y.shape)
    l = model.encoder(x)
    print("l.shape", l.shape)
    y = model.decoder(l)
    print("y.shape", y.shape)

    print(model)

