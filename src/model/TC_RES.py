# -*- coding: utf-8 -*-
# Author:Wei-Chien Wang

import numpy as np
import os
import torch
import torch.nn as nn
import math


def ACT(act_f):
    if (act_f == "relu"):
        return torch.nn.ReLU()
    elif (act_f == "tanh"):
        return torch.nn.Tanh()
    elif (act_f == "relu6"):
        return nn.ReLU6()
    elif (act_f == "sigmoid"):
        return nn.Sigmoid()
    elif (act_f == "LeakyReLU"):
        return nn.LeakyReLU()
    else:
        print("Doesn't support {0} activation function".format(act_f))


class TC_ResNet(nn.Module):
    """

    """

    def __init__(self, input, num_classes, n_blocks, n_channels, scope,
                 pool=None):
        super(TC_ResNet, self).__init__()

        self.endpoints = dict()
        self.F = input.shape[0]  # freq. narrow band
        self.L = input.shape[1]  # 音訊的長度
        self.C = input.shape[2]  # channels
        self.input = torch.reshape(input, (-1, self.F, self.L, self.C))
        assert n_blocks == len(n_channels)-1

        self.conv2d_1 = nn.Conv2d(in_channels=10)


        pass

    def forward(self):
        pass


if __name__ == "__main__":
    #####test phasea
    x = torch.tensor(np.ones((10, 1, 1, 257))).float()
    model_dict = {
        "frequency_bins": [0, 257],
        "encoder": [1024, 512, 256, 32],
        "decoder": [32, 256, 512, 1024, 1],
        "encoder_filter": [[1, 3], [1, 3], [1, 3], [1, 3]],
        "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 3], [1, 1]],
        "encoder_act": "relu",
        "decoder_act": "relu",
        "dense": [16],
    }

    pass
