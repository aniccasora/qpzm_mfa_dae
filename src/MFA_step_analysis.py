"""
for windows environment
"""
import argparse
import os
from os.path import join as pjoin
import time
from utils import misc
import torch
from torch.autograd import Variable
from datetime import datetime
import numpy as np
from model import DAE_F, _DAE_R , DAE_C
#from model import DAE_C_v2 as DAE_C  #, depthwise好像不太優
#from model import DAE_C_v3 as DAE_C  # depthwise & 週期學習, 有nan問題
#from model import DAE_C_v3_1 as DAE_C  # 週期學習，使用 train_v3
# from model import DAE_TC as DAE_C
import train as train
#import train_v3 as train  # 因應週期性學習用
from source_separation import MFA
from dataset.HLsep_dataloader import hl_dataloader, val_dataloader
from dataset.HLsep_dataloader import hl_dataloader_entire_spec
import scipy.io.wavfile as wav
from tqdm import tqdm

# parser#
parser = argparse.ArgumentParser(description='PyTorch Source Separation')
parser.add_argument('--model_type', type=str, default='DAE_C', help='model type')
parser.add_argument('--data_feature', type=str, default='lps', help='lps or wavform')

parser.add_argument('--pretrained', default=False, help='load pretrained model or not')
parser.add_argument('--pretrained_path', type=str, default=None, help='pretrained_model path')
parser.add_argument('--trainOrtest', type=str, default="train", help='status of training')
# training hyperparameters
parser.add_argument('--optim', type=str, default="Adam", help='optimizer for training',
                    choices=['RMSprop', 'SGD', 'Adam'])
parser.add_argument('--batch_size', type=int, default=64, help='input batch size for training (default: 64)')
parser.add_argument('--lr', type=int, default=1e-3, help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--CosineAnnealingWarmRestarts', type=bool, default=False,
                    help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--epochs', type=int, default=150, help='number of epochs to train (default: 10)')
parser.add_argument('--grad_scale', type=float, default=8, help='learning rate for wage delta calculation')
parser.add_argument('--seed', type=int, default=11, help='random seed (default: 1)')

parser.add_argument('--log_interval', type=int, default=100,
                    help='how many batches to wait before logging training status')
parser.add_argument('--test_interval', type=int, default=1, help='how many epochs to wait before another test')
parser.add_argument('--logdir', default='log/', help='folder to save to the log')
parser.add_argument('--decreasing_lr', default='200,250', help='decreasing strategy')
# MFA hyperparameters
parser.add_argument('--source_num', type=str, default=3, help='number of separated sources')
parser.add_argument('--clustering_alg', type=str, default='NMF', help='clustering algorithm for embedding space')
parser.add_argument('--wienner_mask', type=bool, default=True, help='wienner time-frequency mask for output')

# =================================================
# 額外的設定
# 使用自取的資料夾存放
parser.add_argument('--fix_logger_folder_name', default=True, action='store_true', help='fixed logger folder name.')
parser.add_argument('--logger_folder_name', type=str, default='MFA_ANA')
# 改檔要為 True
parser.add_argument('--always_train', type=bool, default=True, help='測試用，如果都單獨測試一個可設定')

# 開啟多筆測試
parser.add_argument('--use_test_PR_list', type=bool, default=False, help='使用 test_PR list測試')
parser.add_argument('--test_PR_list', default='5 10 20 30 40 50 60 70 80 90 95', type=str, help='測試用，用於分離階段的閥值列表')

# 要單獨測 --use_test_PR_list, 設定 False
parser.add_argument('--dis_PR', default=30, type=int, help="標記近最近權重的百分之多少")
# 測試存放資料夾名，"另存檔案夾名如空 則直接存(後方加上斜線)"
parser.add_argument('--lab_dir_name', default='4_1_10_4/', type=str, help="另存檔案夾名如空 則直接存(後方加上斜線)")

# depthwise convolution K
parser.add_argument('--depthwise_K', default=2, type=int, help="depthwise convolution 超參數。")

# 使用整張 spec 作為輸入。 尚未完成
parser.add_argument('--entire_spec_mode', default=False, type=bool, help="depthwise convolution 超參數。")

# 使用 class 3 時採用 何種關閉策略。(近似權重會被標記為 class 3)
parser.add_argument('--suppress_close_node', default=True, type=bool, help="True ，c3權重歸零;"
                                                                            "False，c3保留權重。")
# =================================================

args = parser.parse_args()

# 測試用參數
args.test_PR_list = [int(_) for _ in args.test_PR_list.split(' ')]

args.cuda = torch.cuda.is_available()

misc.logger.init(args.logdir, args.logger_folder_name + '.txt')

if args.fix_logger_folder_name:
    _log_path = os.path.join(args.logdir[:-1], args.logger_folder_name)
    print("log at:{}".format(_log_path))
    args.logdir = os.path.join(args.logdir[:-1], args.logger_folder_name)
    misc.logger.info("fixed logger folder!")
else:
    starttime = time.time()
    current_time = datetime.now().strftime('%Y%m%d_%H%M')
    args.logdir = args.logdir + str(args.model_type) + "_" + str(current_time)
    print("log at:{}".format(args.logdir))
misc.ensure_dir(args.logdir)

logger = misc.logger.info
logger("=================FLAGS==================")
for k, v in args.__dict__.items():
    logger('{}: {}'.format(k, v))
logger("========================================")

"""
if args.seed is not None:
    random.seed(args.seed)
    cudnn.deterministic=None
    ngpus_per_node = torch.cuda.device_count()
"""
# build model
decreasing_lr = list(map(int, args.decreasing_lr.split(',')))
logger('decreasing_lr: ' + str(decreasing_lr))
best_acc, old_file = 0, None
per_save_epoch = 30
t_begin = time.time()
grad_scale = args.grad_scale
# model dictionary
old_DAE_C_dict = {
    "frequency_bins": [0, 300],
    "encoder": [32, 16, 8],
    "decoder": [8, 16, 32, 1],
    "encoder_filter": [[1, 3], [1, 3], [1, 3]],
    "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
    "encoder_act": "relu",
    "decoder_act": "relu",
    "dense": [],
}
# The 蓋
# 改 frequency_bins 記得改 FFT_dict...
DAE_C_dict = {
    "frequency_bins": [0, 300],
    "encoder": [32, 16, 8],
    "decoder": [8, 16, 32, 1],
    "encoder_filter": [[1, 3], [1, 3], [1, 3]],
    "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
    "encoder_act": "relu",  # sigmoid
    "decoder_act": "relu",  # sigmoid
    "dense": [],
}
DAE_F_dict = {
    "frequency_bins": [0, 300],
    "encoder": [1024, 512, 256, 128],
    "decoder": [256, 512, 1024, 1025],
    "encoder_act": "relu",
    "decoder_act": "relu",
}

Model = {
    'DAE_C': DAE_C.autoencoder,
    'DAE_F': DAE_F.autoencoder,
    'DAE_R': _DAE_R.autoencoder,
}

model_dict = {
    'DAE_C': DAE_C_dict,
    'DAE_F': DAE_F_dict,
    'DAE_R': DAE_C_dict
}

FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 2048,
    'Hop_length': 128,
    'Win_length': 2048,
    'normalize': True,
}

# 原本的參數 before v3
before_v3_FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 2048,
    'Hop_length': 128,  # 128
    'Win_length': 2048,
    'normalize': True,
}

#
# eval by matlab
# Resolution: 128 ms
# Overlap: 80%
NEW_FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 1024,
    'Hop_length': 205,
    'Win_length': 1024,
    'normalize': True,
}

# declare model
net = Model[args.model_type](model_dict=model_dict[args.model_type], args=args, logger=logger).cuda()

torch.manual_seed(args.seed)
device = None
if args.cuda:
    torch.cuda.manual_seed(args.seed)
    net.cuda()
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

if __name__ == "__main__":

    history_list = \
        [r".\dataset\training_noisy_心肺\0dB\0_0.wav",  # 0 0_0
         r'.\dataset\audio_Healthy\102_1b1_Ar_sc_Meditron.wav',  # 1
         r'.\dataset\audio_Healthy\121_1b1_Tc_sc_Meditron.wav',  # 2
         r'.\dataset\audio_Healthy\121_1p1_Tc_sc_Meditron.wav',  # 3
         r'.\4_1.wav',  # 4
    ]

    # 一次跑整個資料夾下的音訊
    # *** 記得調整 資料夾名(--lab_dir_name) 這樣才不會跟舊的混到一樣資料夾 ***
    #_fetch_dir = r"D:\Git\qpzm_mfa_dae\src\dataset\training_noisy_心肺\0dB"
    _fetch_dir = r"D:\Git\qpzm_mfa_dae\src\dataset\audio_Healthy"

    # 覆蓋 hard-code 變數，一次跑多檔案
    # history_list = [pjoin(_fetch_dir, _)for _ in os.listdir(_fetch_dir)]

    # 如 history_list 只想測試單筆可以改成 [history_list[4]]，in 後面 改 range(1)
    #for sep_audio in history_list:  # 迭代版
    for sep_audio in range(1):  # 指定單獨跑
        test_filelist = [history_list[4]]  # 有修改記得 args.always_train 要給 True
        #test_filelist = [sep_audio]  # 迭代版


        test_filename = test_filelist[0].rsplit(os.path.sep)[-1].split('.')[0]  #"102_1b1_Ar_sc_Meditron"
        MODEL_SAVE_NAME = test_filename

        model_save_dir = 'model_save'

        # 檢查是否存在訓練過的 pt檔案，換權重傳薪訓練
        exists_pt = os.path.isfile(os.path.join(model_save_dir, f'{MODEL_SAVE_NAME}.pt'))
        logger(f"\n>>> exists_pt: {exists_pt}")

        always_train = args.always_train
        print(f"\nalways_train = {always_train}\n")
        # train
        if always_train:  # not exists_pt
            if args.entire_spec_mode:
                train_loader = hl_dataloader_entire_spec(test_filelist, batch_size=1, shuffle=False, num_workers=0,
                                         pin_memory=True, FFT_dict=FFT_dict, args=args)
            else:
                train_loader = hl_dataloader(test_filelist, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                         pin_memory=True, FFT_dict=FFT_dict, args=args)
            # train，沒錯 就是要重新 init 新的net
            net = Model[args.model_type](model_dict=model_dict[args.model_type], args=args, logger=logger).cuda()
            net = train.train(train_loader, net, args, logger)

            # reload best.pt and resave with new name
            misc.ensure_folder(model_save_dir, logger=misc.logger)
            # reload
            net.load_state_dict(torch.load(pjoin(_log_path, 'best.pth')))
            # resave with new name
            torch.save(net.state_dict(), os.path.join(model_save_dir, f'{MODEL_SAVE_NAME}.pt'))


        # data loader
        data_loader = hl_dataloader(test_filelist, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                          pin_memory=True, FFT_dict=FFT_dict, args=args)


        # chk save folder
        model_save_dir = 'model_save'
        misc.ensure_folder(model_save_dir, logger=misc.logger)

        # Reload model
        net.load_state_dict(torch.load(os.path.join(model_save_dir, f'{MODEL_SAVE_NAME}.pt')))
        net.to(device)

        # logger('\n==== <net_structure> ====')
        # logger('padding tuple meaning:{left, right, top, bottom}')
        # logger('conv 使用 寬型(1x3)kernel 所以 padding 只需一邊(橫向)，特別的是統一pad在右邊')
        # logger(net)
        # logger('==== <net_structure> ====\n')
        outdir = "{0}/test_".format(args.logdir)


        # PR_set
        PR_SET = []

        if args.use_test_PR_list:
            PR_SET = args.test_PR_list
        else:
            assert type(args.dis_PR) == int
            PR_SET.append(args.dis_PR)

        for PR_val in PR_SET:
            args.dis_PR = PR_val

            # Source Separation by MFA analysis.
            mfa_old = MFA.MFA_source_separation(net, FFT_dict=FFT_dict, args=args,
                                                logger=logger, use_class3=False)
            mfa_new = MFA.MFA_source_separation(net, FFT_dict=FFT_dict, args=args,
                                                logger=logger, use_class3=True)

            # for mfa in [mfa_new, mfa_old]:
            for mfa in [mfa_old]:
                for key, val in FFT_dict.items():
                    logger(key + ", " + str(val))
                for test_file in test_filelist:
                    lps, phase, mean, std = val_dataloader(test_file, FFT_dict, logger)
                    mfa.source_separation(np.array(lps), np.array(phase), np.array(mean), np.array(std), filedir=outdir,
                                          filename=test_filename)



    # log 重新編碼
    _log_txt = os.path.join('log', args.logger_folder_name + '.txt')
    with open(_log_txt, encoding='big5') as inFile:
        content = inFile.read()
    with open(_log_txt, mode='w+', encoding='utf-8') as outFile:
        outFile.write(content)

    print("\n運行音訊:", test_filelist)
    print("K:", args.depthwise_K)

