# -*- coding: utf-8 -*-
# Author:Wei-Chien Wang

import sys
import torch
import numpy as np
import librosa
import scipy
from utils.clustering_alg import K_MEANS,  NMF_clustering
import os
import scipy.io.wavfile as wav
from utils.signalprocess import lps2wav
sys.path.append('../')


class MFA_source_separation(object):

    """
    MFA analysis for unsupervised monoaural blind source separation.
        This function separate different sources by unsupervised manner depend on different source's periodicity properties.
    Arguments:
        model: deep autoencoder for source separation.
        source number: separated source number.
        clustering _alg: clustering algorithm for MFA analysis.
        wienner_mask: if True the output is mask by constructed ratio mask.
        FFT_dict: fouier transform parameters.
        use_class3: 使用到類別3。(過於相近的權重不關閉 node)
    """
    def __init__(self, model=None, FFT_dict=None, args=None, logger=None, use_class3=False):
        self.model = model
        self.source_num = args.source_num
        self.clustering_alg = args.clustering_alg
        self.wienner_mask = args.wienner_mask
        self.FFT_dict = FFT_dict
        self.args = args
        self.logger = logger
        self.use_class3 = use_class3

    def FFT_(self, input):
        epsilon = np.finfo(float).eps
        frame_num = input.shape[1]
        encoding_shape = input.shape[0]
        FFT_result = np.zeros((encoding_shape, int(frame_num/2+1)))

        for i in range(0, encoding_shape):
            fft_r = librosa.stft(input[i, :], n_fft=frame_num, hop_length=frame_num+1, window=scipy.signal.hamming)
            fft_r = fft_r+ epsilon
            FFT_r = abs(fft_r)**2
            FFT_result[i] = np.reshape(FFT_r, (-1,))

        return FFT_result
    

    def freq_modulation(self, source_idx, label, encoded_img):
        """
          This function use to modulate latent code.
          Arguments:
            source_idx: source no. 
            Encoded_img: latent coded matrix. Each dimension represnet as (encoding_shape, frame_num)
            label: latent neuron label.
        """
        frame_num = encoded_img.shape[0]
        encoding_shape = encoded_img.shape[1]  # 2400 那個維度，latent 的數量。
        # minimun value of latent unit.
        min_value = torch.min(encoded_img)
        mean_value = torch.mean(encoded_img)  # test 是否有關對用
        if not self.use_class3:
            # print("使用舊有套路則一關閉神經元")
            for k in range(0, encoding_shape):
                if(label[k] != source_idx):
                    # deactivate neurons
                    encoded_img[:, k] = min_value  # (encoding_shape,frame_num)

        # else:  # --EDI--TED--: 針對類別 "2"，不關閉神經元
        #     print("不關閉過於權重類似神經元")
        #     for k in range(0, encoding_shape):
        #         if label[k] == 2:  # 差距過小，為 "2" 類別。
        #             continue  # 啥都不做
        #
        #         if label[k] != source_idx:
        #             # deactivate neurons
        #             encoded_img[:,k] = min_value  # (encoding_shape,frame_num)
        else:  # MODIFIED: 針對類別 "2"  ~~~處理神經元~~~~ v
            cnt_all = 0
            highlight_processTimes = 0
            for k in range(0, encoding_shape):
                cnt_all += 1

                if not self.args.suppress_close_node:
                    # 過近兩者保留
                    if label[k] == 2:  # 差距過小，為 "2" 類別。
                        highlight_processTimes += 1
                        continue  # 保持權重

                    if label[k] != source_idx:
                        # deactivate neurons
                        encoded_img[:, k] = min_value
                else:
                    #  過近關閉
                    if label[k] == 2:  # 差距過小，為 "2" 類別。
                        highlight_processTimes += 1
                        encoded_img[:, k] = min_value
                        continue

                    if label[k] != source_idx:
                        # deactivate neurons
                        encoded_img[:, k] = min_value
            # end for
        # else end

        return encoded_img


    def MFA(self, input, source_num=2):
        """
          latent space 的調製頻率分析。
          Note: input 的各維度意義。 (frame number, encoded neuron's number).

          Arguments:
              input: 2D Tensor.
              source_num: int.
                  source quantity.
        """
        encoded_dim = input.shape[1]
        # Period clustering
        fft_bottleneck = self.FFT_(input.T)#(fft(encoded, frame_num))

        if self.clustering_alg == "K_MEAMS":
            k_labels, k_centers = K_MEANS.create_cluster(np.array(fft_bottleneck[:, 2:50]), source_num)
        else:
            _, _, k_labels, _ = NMF_clustering.\
                basis_exchange(np.array(fft_bottleneck[:, 3:]).T,
                               np.array(fft_bottleneck[:, 3:]),
                               np.array([source_num]),
                               segment_width=1,
                               logger=self.logger,
                               use_class3=self.use_class3,
                               dis_PR=self.args.dis_PR)

        return k_labels


    def source_separation(self, input, phase, mean, std, filedir, filename):
        """
          main function for blind source separation.
          Argument:
              input: log power spectrum (lps).
              phase: phase is used to inverse lps to wavform.
              mean: mean value of lps.
              std: variance of lps.
              filedir: directory for separated sources.
              filename: separated sources name.
        """
        feature_dim = self.FFT_dict['frequency_bins'][1] - self.FFT_dict['frequency_bins'][0]

        if self.args.model_type in ["DAE_C", "DAE_R"]:
            x = np.reshape(input.T, (-1, 1, 1, int(self.FFT_dict['FFTSize'] / 2 + 1)))[:, :, :,
                self.FFT_dict['frequency_bins'][0]:self.FFT_dict['frequency_bins'][1]]
        else:
            x = input.T[:, self.FFT_dict['frequency_bins'][0]:self.FFT_dict['frequency_bins'][1]]
        x = torch.tensor(x).float().cuda()
        sources = torch.unsqueeze(torch.zeros_like(x), 0)

        # Encode input
        ___ = self.model.encoder(x)

        if isinstance(___, tuple):
            assert len(___) == 3
            latent_code, _, _ = ___
        else:
            latent_code = ___

        # MFA analysis for identifying latent neurons's label
        label = self.MFA(latent_code.cpu().detach().numpy())  # EDITED: label 類別回傳至此。

        # Reconstruct input
        sources[0] = self.model.decoder(latent_code)

        # Discriminate latent code for different sources.
        # 開始音源分離步驟.
        for source_idx in range(0, self.source_num):
            # 分類神經元，這邊會關閉神經元
            _d_sources_shape = sources.shape
            y_s = self.freq_modulation(source_idx, label, latent_code)
            # unsqueeze : 插入 "一個dim" ，輸出的維度(改變維度)
            # >>> ex: [2,3] 插在0 => [1,2,3]; [2,3] 插在1=> [2,1,3]
            sources = torch.cat((sources, torch.unsqueeze(self.model.decoder(y_s), 0)), 0)
        # [4, x ... x ...]
        sources = torch.squeeze(sources).permute(0, 2, 1).detach().cpu().numpy()
        # Source separation
        for source_idx in range(0, self.source_num+1):
            sources[source_idx, :, :] = np.sqrt(10 ** ((sources[source_idx, :, :] * std[
                                                                                    self.FFT_dict['frequency_bins'][0]:
                                                                                    self.FFT_dict['frequency_bins'][1],
                                                                                    :]) + mean[self.FFT_dict[
                                                                                                   'frequency_bins'][0]:
                                                                                               self.FFT_dict[
                                                                                                   'frequency_bins'][1],
                                                                                          :]))

        # Inverse separated sources of log power spectrum to waveform.
        input = np.sqrt(10**(input*std+mean))

        for source_idx in range(0, self.source_num+1):
            Result = np.array(input)
            if self.wienner_mask == True:
                #  Reconstruct original signal
                if source_idx == 0:
                    Result[self.FFT_dict['frequency_bins'][0]:self.FFT_dict['frequency_bins'][1], :] = sources[0, :, :]
                else:
                    Result[self.FFT_dict['frequency_bins'][0]:self.FFT_dict['frequency_bins'][1], :] = 2*(sources[source_idx, :, :]/(np.sum(sources[1:, :, :], axis=0)))*sources[0, :, :]
            else:#Wienner_mask==False
                Result[self.FFT_dict['frequency_bins'][0]:self.FFT_dict['frequency_bins'][1], :] = np.array(sources[source_idx, :, :])
            R = np.multiply(Result, phase)
            result = librosa.istft(R, hop_length=self.FFT_dict['Hop_length'], win_length=self.FFT_dict['Win_length'], window=scipy.signal.hamming, center=False)
            result = np.int16(result*32768)

            # 處理存放資料夾
            if source_idx == 0:
                result_path = "{0}reconstruct/{1}".format(filedir, self.args.lab_dir_name)
            else:
                result_path = "{0}source{1}/{2}".format(filedir, source_idx, self.args.lab_dir_name)
            if not os.path.exists(result_path):
                os.makedirs(result_path)

            if self.use_class3:
                if filename.find("_use_class3") == -1:  # 防止 append過多次
                    filename = filename + "_use_class3"
            else:
                if filename.find("_old_method") == -1:  # 防止 append過多次
                    filename = filename + "_old_method"
            # 檔案名稱加上 資料夾前綴、後綴
            save_file_name = "{0}{1}_{2}".format(result_path, source_idx, filename)

            if self.wienner_mask is False:
                # 加上額外名稱
                save_file_name = f"{save_file_name}_close_wienner_mask"
            else:
                pass

            save_file_name = f"{save_file_name}_PR{self.args.dis_PR}"

            wav.write(f'{save_file_name}.wav', self.FFT_dict['sr'], result)

            # wav.write("{0}{1}.wav".format(result_path,  filename), self.FFT_dict['sr'], result)
