"""
分析 latent code 週期用的
"""
import argparse
import time
from utils import misc
import torch

from datetime import datetime
import numpy as np
from model import DAE_C, DAE_F
import train_v3
import librosa,scipy
from matplotlib.ticker import MaxNLocator
from dataset.HLsep_dataloader import hl_dataloader
import matplotlib.pyplot as plt

# parser#

parser = argparse.ArgumentParser(description='PyTorch Source Separation')
parser.add_argument('--model_type', type=str, default='DAE_C', help='model type')
parser.add_argument('--data_feature', type=str, default='lps', help='lps or wavform')

parser.add_argument('--pretrained', default=False, help='load pretrained model or not')
parser.add_argument('--pretrained_path', type=str, default=None, help='pretrained_model path')
parser.add_argument('--trainOrtest', type=str, default="train", help='status of training')
# training hyperparameters
parser.add_argument('--optim', type=str, default="Adam", help='optimizer for training',
                    choices=['RMSprop', 'SGD', 'Adam'])
parser.add_argument('--batch_size', type=int, default=32, help='input batch size for training (default: 64)')
parser.add_argument('--lr', type=int, default=1e-4, help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--CosineAnnealingWarmRestarts', type=bool, default=False,
                    help='initial learning rate for training (default: 1e-3)')
parser.add_argument('--epochs', type=int, default=50, help='number of epochs to train (default: 10)')
parser.add_argument('--grad_scale', type=float, default=8, help='learning rate for wage delta calculation')
parser.add_argument('--seed', type=int, default=117, help='random seed (default: 1)')

parser.add_argument('--log_interval', type=int, default=100,
                    help='how many batches to wait before logging training status')
parser.add_argument('--test_interval', type=int, default=1, help='how many epochs to wait before another test')
parser.add_argument('--logdir', default='log/', help='folder to save to the log')
parser.add_argument('--decreasing_lr', default='200,250', help='decreasing strategy')
# MFA hyperparameters
parser.add_argument('--source_num', type=str, default=3, help='number of separated sources')
parser.add_argument('--clustering_alg', type=str, default='NMF', help='clustering algorithm for embedding space')
parser.add_argument('--wienner_mask', type=bool, default=True, help='wienner time-frequency mask for output')

args = parser.parse_args()
args.cuda = torch.cuda.is_available()
misc.logger.init(args.logdir, 'pycharm_log')
logger = misc.logger.info

starttime = time.time()
current_time = datetime.now().strftime('%Y%m%d_%H%M')
args.logdir = args.logdir + str(args.model_type) + "_" + str(current_time)

misc.ensure_dir(args.logdir)
logger("=================FLAGS==================")
for k, v in args.__dict__.items():
    logger('{}: {}'.format(k, v))
logger("========================================")

"""
if args.seed is not None:
    random.seed(args.seed)
    cudnn.deterministic=None
    ngpus_per_node = torch.cuda.device_count()
"""
# build model
decreasing_lr = list(map(int, args.decreasing_lr.split(',')))
logger('decreasing_lr: ' + str(decreasing_lr))
best_acc, old_file = 0, None
per_save_epoch = 30
t_begin = time.time()
grad_scale = args.grad_scale
# model dictionary
DAE_C_dict = {
    "frequency_bins": [0, 300],
    "encoder": [32, 16, 8],
    "decoder": [8, 16, 32, 1],
    "encoder_filter": [[1, 3], [1, 3], [1, 3]],
    "decoder_filter": [[1, 3], [1, 3], [1, 3], [1, 1]],
    "encoder_act": "relu",
    "decoder_act": "relu",
    "dense": [],
}
DAE_F_dict = {
    "frequency_bins": [0, 300],
    "encoder": [1024, 512, 256, 128],
    "decoder": [256, 512, 1024, 1025],
    "encoder_act": "relu",
    "decoder_act": "relu",
}

Model = {
    'DAE_C': DAE_C.autoencoder,
    'DAE_F': DAE_F.autoencoder,
}

model_dict = {
    'DAE_C': DAE_C_dict,
    'DAE_F': DAE_F_dict
}
FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 2048,
    'Hop_length': 128,
    'Win_length': 2048,
    'normalize': True,
}
# declare model
net = Model[args.model_type](model_dict=model_dict[args.model_type], args=args, logger=logger).cuda()

torch.manual_seed(args.seed)

if args.cuda:
    torch.cuda.manual_seed(args.seed)
    net.cuda()

if __name__ == "__main__":

    # data loader
    heart_filelist = ["./data/123_pure_heart_manul_16bit.wav"]
    heart_filename = "123_pure_heart"
    outdir = "{0}/test_".format(args.logdir)
    train_loader = hl_dataloader(heart_filelist, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                 pin_memory=True, FFT_dict=FFT_dict, args=args)
    # train
    heart_net = train_v3.train(train_loader, net, args, logger)

    # 取得心臟 dl
    heart_dataloader = hl_dataloader(heart_filelist, batch_size=args.batch_size, shuffle=False, num_workers=0,
                                    pin_memory=True, FFT_dict=FFT_dict, args=args)

    # 丟入已經訓練好的 encoder
    for frame in heart_dataloader:
        frame = frame.detach().clone().type(torch.float)
        latent_output = heart_net.encoder(frame.cuda())

    fft_result = []

    frame_num = latent_output.shape[1]
    epsilon = np.finfo(float).eps
    for i in range(latent_output.shape[0]):
        # origin time-series
        ts = latent_output[i].detach().cpu()
        # fft
        fft_ = librosa.stft(ts[:].numpy(), n_fft=frame_num, hop_length=frame_num + 1, window=scipy.signal.hamming)
        fft_ = fft_ + epsilon
        fft_ = abs(fft_) ** 2

        fft_result.append(fft_)

    fft_result = np.array(fft_result)
    fft_result = np.squeeze(fft_result)
    partial_fft_ = fft_result[:, FFT_dict['frequency_bins'][0]:FFT_dict['frequency_bins'][1]]
    partial_fft_softmax_ = scipy.special.softmax(partial_fft_, axis=1)

    # =============== lung ===================
    # data loader
    lung_filelist = ["./filtered_wav/209_pure_lung_16bit.wav"]
    lung_filename = "209_pure_lung"
    outdir = "{0}/lung_".format(args.logdir)
    train_loader = hl_dataloader(lung_filelist, batch_size=args.batch_size, shuffle=True, num_workers=0,
                                 pin_memory=True, FFT_dict=FFT_dict, args=args)
    # train
    lung_net = train_v3.train(train_loader, net, args, logger)

    # 取得肺臟 dl
    lung_dataloader = hl_dataloader(lung_filelist, batch_size=args.batch_size, shuffle=False, num_workers=0,
                                     pin_memory=True, FFT_dict=FFT_dict, args=args)

    # 丟入已經訓練好的 encoder
    for frame in lung_dataloader:
        frame = frame.detach().clone().type(torch.float)
        latent_output = lung_net.encoder(frame.cuda())

    fft_result = []

    frame_num = latent_output.shape[1]
    epsilon = np.finfo(float).eps
    for i in range(latent_output.shape[0]):
        # origin time-series
        ts = latent_output[i].detach().cpu()
        # fft
        fft_ = librosa.stft(ts[:].numpy(), n_fft=frame_num, hop_length=frame_num + 1, window=scipy.signal.hamming)
        fft_ = fft_ + epsilon
        fft_ = abs(fft_) ** 2

        fft_result.append(fft_)

    fft_result = np.array(fft_result)
    fft_result = np.squeeze(fft_result)
    partial_fft_ = fft_result[:, FFT_dict['frequency_bins'][0]:FFT_dict['frequency_bins'][1]]
    lung_partial_fft_softmax_ = scipy.special.softmax(partial_fft_, axis=1)

    print("end")