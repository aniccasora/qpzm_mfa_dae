import numpy as np
from scipy.stats import entropy
import matplotlib.pyplot as plt

def L(tau, Hz, Hp, lambda_p):
    """
    measure the effectiveness of temperature parameter.
    用來評估 tau 參數好不好 用的。
    """
    return (entropy(Hz)-entropy(Hp))**2 + lambda_p*(Hz**2)


def softmax(d, tau=1):
    assert d.ndim == 1  # 目前只處理一維向量

    numerator = np.exp(d/tau)  # 分子
    denominator = np.sum(np.exp(d/tau))  # 分母

    return numerator/denominator


def Zd(x):
    assert x.ndim == 1
    x = np.array(x)
    numerator = x+2*np.min(x) + 0.01
    denominator = np.sum(x+2*np.min(x) + 0.01)

    return numerator/denominator

def get_tau(tau, x, lambda_p):
    _Zd = Zd(x)
    numerator = np.sum(_Zd*np.exp(_Zd/tau))
    denominator_L = np.sum(np.exp(_Zd/tau))
    denominator_R = (np.log(np.sum(np.exp(_Zd/tau))))-(entropy(_Zd/(1+lambda_p)))

    return numerator/(denominator_L*denominator_R)

if __name__ == "__main__":
    H = np.load("H.npy")


    x = H[1]

    lambda_p = 1

    tau = 1  # 初始狀態
    tau_opti = tau
    epsilon = 10e-9  # 差距閥

    iter_times= 0
    while True:
        iter_times += 1
        print("迭代次數:", iter_times)
        tau_opti = tau
        tau = get_tau(tau_opti, x, lambda_p)  # 更新 tau

        if abs(tau_opti - tau) < epsilon:
            break

    new_x = x/tau_opti
    print("new entropy:", softmax(new_x))

    draws_data = [
        {'title': 'x', 'data': x},
        {'title': 'softmax(x)', 'data': softmax(x)},
        {'title': 'new_x', 'data': new_x},
        {'title': 'softmax(new_x)', 'data': softmax(new_x)},
        {'title': 'softmax - 2', 'data': softmax(x/2)},
    ]

    fig, ax = plt.subplots(len(draws_data), 1)
    for idx, dd in enumerate(draws_data):
        ax[idx].plot(dd['data'])
        ax[idx].set_title(dd['title'])
    # ax[0].plot(x)
    # ax[0].set_title("x")
    #
    # ax[1].plot(softmax(x))
    # ax[1].set_title("softmax(x)")
    #
    # ax[2].plot(new_x)
    # ax[2].set_title("new_x")
    #
    # ax[3].plot(softmax(new_x))
    # ax[3].set_title("softmax(new_x)")

    plt.tight_layout()
    plt.show()
