import argparse
import os
import time
from utils import misc
import torch
from torch.autograd import Variable
from datetime import datetime
import numpy as np
from model import DAE_C, DAE_F, _DAE_R
import train_v3
from source_separation import MFA
from dataset.HLsep_dataloader import hl_dataloader, val_dataloader
import scipy.io.wavfile as wav
from tqdm import tqdm
from utils.signalprocess import wav2lps, lps2wav, wav_read


FFT_dict = {
    'sr': 8000,
    'frequency_bins': [0, 300],
    'FFTSize': 2048,
    'Hop_length': 128,
    'Win_length': 2048,
    'normalize': True,
}

if __name__ == "__main__":
    # ground truth 的 兩個音訊路徑 (心、肺)
    ground_truth_sep = {
        'heart': r"C:\Users\Anicca\Desktop\0_0的標準分離\0_0_心跳_ground_truth.wav",
        'lung': r"C:\Users\Anicca\Desktop\0_0的標準分離\0_0_肺音_ground_truth.wav",
    }
    # 原有分法 的 兩個音訊路徑 (心、肺)
    ori_method_sep = {
        'heart': r"",
        'lung': r"",
    }

    spec_ground_truth_heart, _, _, _ = wav2lps(ground_truth_sep['heart'], FFT_dict['FFTSize'],
                                     FFT_dict['Hop_length'],
                                     FFT_dict['Win_length'],
                                        FFT_dict['normalize'])